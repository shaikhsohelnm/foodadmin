import "./App.css";
import Login from "./pages/Login";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import { useEffect } from "react";
import { useSelector } from "react-redux";

function App() {
  const token = useSelector((state) => state.authReducer?.authToken);

  useEffect(() => {
    console.log(token);
  }, []);

  // const token = "fsdfsdf";

  return (
    <BrowserRouter>
      {token ? (
        <Routes>
          <Route path="/" element={<Navigate to="/dashboard" />} />
          <Route path="/dashboard" element={<Dashboard />} />
        </Routes>
      ) : (
        <Routes>
          <Route path="/" element={<Navigate to="/login" />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      )}
    </BrowserRouter>
  );
}

export default App;
