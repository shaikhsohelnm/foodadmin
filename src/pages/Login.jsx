import React, { useState, useEffect } from "react";
import { Button, Checkbox, Form, Input } from "antd";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setAuthToken, setUserProfile } from "../redux/actions/AuthActions";
import store from "../redux/store";
import { useNavigate } from "react-router-dom";

function Login() {
  const navigate = useNavigate();
  const [userData, setUserData] = useState();
  const dispatch = useDispatch();

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleformsubmit = async (values) => {
    const form = new FormData();
    form.append("email", values.email);
    form.append("password", values.password);
    form.append("remember", values.remember);

    try {
      const response = await axios.post(
        "http://127.0.0.1:8000/api/login",
        form
      );
      setUserData(response.data);
      console.log(response.data.user.usertype);
      setUserData(response.data);

      setusertokenprofile(
        response.data.authorisation.token,
        response.data.user.usertype
      );
      navigate("/dashboard");
    } catch (error) {
      console.log(error);
    }
  };

  const setusertokenprofile = (token, profile) => {
    dispatch(setAuthToken(token));
    dispatch(setUserProfile(profile));
  };

  store.subscribe(() => console.log(store.getState()));

  useEffect(() => {
    console.log(store.getState());
  }, []);

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      style={{
        maxWidth: 600,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={handleformsubmit}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            required: true,
            message: "Please input your Email!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Login;
