export const setAuthToken = (token) => {
  if (token) {
    localStorage.setItem("access_token", token);
    sessionStorage.setItem("access_token", token);
  }

  return {
    type: "SET_AUTH_TOKEN",
    payload: token,
  };
};

export const setUserProfile = (profile) => {
  if (profile) {
    localStorage.setItem("profile", JSON.stringify(profile));
    sessionStorage.setItem("profile", JSON.stringify(profile));
  }

  return {
    type: "SET_USER_PROFILE",
    payload: profile,
  };
};

export const logout = () => ({
  type: "LOGOUT",
});

// const savedProfile = JSON.parse(localStorage.getItem("profile"));

export const getToken = () => {
  let access_token = "";
  if (localStorage.getItem("access_token") !== null) {
    access_token = localStorage.getItem("access_token");
  }

  if (sessionStorage.getItem("access_token") !== null) {
    access_token = sessionStorage.getItem("access_token");
  }
  return { access_token };
};

// const removeToken = () => {
//   sessionStorage.clear();
//   localStorage.clear();
// };
