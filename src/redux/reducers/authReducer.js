const initialState = {
  authToken: null,
  userProfile: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_AUTH_TOKEN":
      return {
        ...state,
        authToken: action.payload,
      };

    case "SET_USER_PROFILE":
      return {
        ...state,
        userProfile: action.payload,
      };

    case "LOGOUT":
      return {
        ...state,
        authToken: null,
        userProfile: null,
      };

    default:
      return state;
  }
};

export default authReducer;
